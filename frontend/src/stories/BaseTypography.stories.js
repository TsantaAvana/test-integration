import BaseTypography from "../components/BaseTypography.vue";
export default {
  title: "BaseTypography",
  component: BaseTypography,

  argTypes: {
    text: String,
    type: {
      control: { type: "select" },
      options: ["paragraph", "title", "subtitle", "caption"],
    },
    color: "dark",
    upperCase: Boolean,
  },
};

const Template = (args) => ({
  components: { BaseTypography },
  setup() {
    return {
      args,
    };
  },
  template: "<BaseTypography v-bind='args' />",
});
export const Default = Template.bind({});

Default.args = {
  text: "Voici un texte en paragraph",
  type: "paragraph",
  color: "dark",
  upperCase: false,
};
export const Title = Template.bind({});

Title.args = {
  text: "Voici un texte en title",
  type: "title",
  color: "dark",
  upperCase: false,
};
export const Subtitle = Template.bind({});

Subtitle.args = {
  text: "Voici un texte en subtitle",
  type: "subtitle",
  color: "dark",
  upperCase: false,
};
export const Caption = Template.bind({});

Caption.args = {
  text: "Voici un texte en caption",
  type: "caption",
  color: "dark",
  upperCase: false,
};
