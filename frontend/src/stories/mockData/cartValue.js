export default {
  id: "123",
  title: "Occurrence title",
  description:
    "ALUMNI VOICES - POSTPONED Finding an internship during covid 19 outbreak and turning it towards a full-time position with RABIGA Margulan",
  website: "https://www.alumnforce.com",
  webinarUrl: "https://www.alumnforce.com",
  beginAt: "2020-03-15 17:10:00",
  endAt: "2020-03-16 17:10:00",
  created: "2020-03-15 17:10:00",
  updated: "2020-03-15 17:10:00",
  cover:
    "https://www.hec.edu/sites/default/files/styles/newsroom_media/public/2022-03/GE_2022_939x495.jpg?itok=fsMLkRJC",
  thumbnail: "/medias/image/thumbnail_27499552060c0d262d8601.jpg",
  embedded: {
    address: {
      venue: "Exhibition center",
      address: "5 Rue Marguerite de Rochechouart",
      address2: "",
      city: "Paris",
      zip: "75009",
      countryIso: "FR",
      latitude: "48.877013",
      longitude: "2.345189",
    },
    author: {
      id: "123",
      name: "John Doe",
      email: "john.doe@alumnforce.com",
      avatar: {
        userId: "123",
        displayName: "John Smith III",
        profilePhoto: "https://cdn-af.example.net/medias/profile/XXXXX.png",
        color: "#AB120F",
        initials: "JS",
        links: {
          self: {
            href: "string",
          },
        },
      },
      links: {
        self: {
          href: "string",
        },
      },
    },
    status: {
      id: 1,
      name: "occurrence_is_waiting_for_validation",
      label: "En attente de validation",
      links: {
        self: {
          href: "string",
        },
      },
    },
    category: {
      id: "123",
      name: "{'en': 'Evenings'}",
    },
    groups: {
      id: "123",
      name: "Contractors",
      description: "Group for contractors users",
      links: {
        self: {
          href: "string",
        },
      },
    },
  },
  isOnSite: true,
  isWebinar: true,
  seatAvailable: "5",
  replayLink: "https://redirect.com",
  webUrl: "https://website.com/foo/bar",
  links: {
    self: {
      href: "string",
    },
  },
};
