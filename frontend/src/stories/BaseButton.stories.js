import BaseButton from "../components/BaseButton.vue";
export default {
  title: "BaseButton",
  component: BaseButton,
  argTypes: {
    text: String,
    variant: {
      control: { type: "select" },
      options: ["outlined", "tonal", "text", "plain"],
    },
    onClick: {},
  },
};

const Template = (args) => ({
  components: { BaseButton },
  setup() {
    return {
      args,
    };
  },
  template: "<BaseButton v-bind='args' />",
});
export const Default = Template.bind({});

Default.args = {
  text: "Voici un texte",
};
