import BaseCard from "../components/BaseCard.vue";
import BaseButton from "../components/BaseButton.vue";
import BaseTypography from "../components/BaseTypography.vue";
import BaseImage from "../components/BaseImage.vue";

import response from "./mockData/cartValue";

export default {
  title: "BaseCard",
  component: BaseCard,
  subcomponents: { BaseButton, BaseTypography, BaseImage },
  argTypes: {
    cartValues: response,
    baseColor: "purple",
    onClick: {},
  },
};

const Template = (args) => ({
  components: { BaseCard },
  setup() {
    return {
      args,
    };
  },
  template: `<BaseCard v-bind='args'></BaseCard > `,
});
export const Default = Template.bind({});

Default.args = {
  cartValues: response,
  isWebinar: true,
};
export const Loading = Template.bind({});

Loading.args = {
  cartValues: null,
};
