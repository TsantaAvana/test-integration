import { createStore } from "vuex";
import apiClient from "@/plugins/axios/index";
import { Card } from "../types/Card";
export interface State {
  cartValues: Card | null;
  isFetchingCartValue: boolean;
}

export default createStore<State>({
  state: {
    cartValues: null,
    isFetchingCartValue: true,
  },
  getters: {},
  mutations: {
    SET_CART_VALUES: (state: State, payload: Card) =>
      (state.cartValues = { ...payload }),
    SET_IS_FETCHING_CARD: (state: State, payload: boolean) =>
      (state.isFetchingCartValue = payload),
  },
  actions: {
    getCartValues: async ({ commit }) => {
      commit("SET_IS_FETCHING_CARD", true);
      const response = await apiClient.get(
        "/api/v2/agenda/occurrence/1?language=auto"
      );
      commit("SET_CART_VALUES", response.data);
      commit("SET_IS_FETCHING_CARD", false);
    },
  },
  modules: {},
});
