import axios, { AxiosInstance } from "axios";
import camelcaseKeys from "camelcase-keys";

const apiClient: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});
apiClient.interceptors.response.use((response) => {
  // /* From snake_case object to camelCase data response */
  response.data = camelcaseKeys(response.data, { deep: true });
  // Return a successful response back to the calling service
  return response;
});

export default apiClient;
