export interface Card {
  id: string;
  title: string;
  description: string;
  website: string;
  webinarUrl: string;
  beginAt: string;
  endAt: string;
  created: string;
  updated: string;
  cover: string;
  thumbnail: string;
  embedded: Embedded;
  isOnSite: boolean;
  isWebinar: boolean;
  seatAvailable: string;
  replayLink: string;
  webUrl: string;
  links: Links5;
}

export interface Embedded {
  address: Address;
  author: Author;
  status: Status;
  category: Category;
  groups: Groups;
}

export interface Address {
  venue: string;
  address: string;
  address2: string;
  city: string;
  zip: string;
  countryIso: string;
  latitude: string;
  longitude: string;
}

export interface Author {
  id: string;
  name: string;
  email: string;
  avatar: Avatar;
  links: Links2;
}

export interface Avatar {
  userId: string;
  displayName: string;
  profilePhoto: string;
  color: string;
  initials: string;
  links: Links;
}

export interface Links {
  self: Self;
}

export interface Self {
  href: string;
}

export interface Links2 {
  self: Self2;
}

export interface Self2 {
  href: string;
}

export interface Status {
  id: number;
  name: string;
  label: string;
  links: Links3;
}

export interface Links3 {
  self: Self3;
}

export interface Self3 {
  href: string;
}

export interface Category {
  id: string;
  name: string;
}

export interface Groups {
  id: string;
  name: string;
  description: string;
  links: Links4;
}

export interface Links4 {
  self: Self4;
}

export interface Self4 {
  href: string;
}

export interface Links5 {
  self: Self5;
}

export interface Self5 {
  href: string;
}
