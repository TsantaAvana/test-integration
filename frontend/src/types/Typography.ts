type Typography = "paragraph" | "title" | "subtitle" | "caption";
export default Typography;
