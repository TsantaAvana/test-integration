const express = require("express");
var cors = require("cors");
const response = require("./response.json");

const app = express();
app.use(cors());
const port = process.env.PORT || 4000;

app.get("/api/v2/agenda/occurrence/:occurence", function (req, res) {
  setTimeout(() => {
    res.send(response);
  }, 1000);
});

app.listen(port);
console.log(`Server started at http://localhost:${port}`);
