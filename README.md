# test-integration

A project that display a card based vuetify-component

# How to start project

Clone project

```shell
cd api/
npm install # install dependencies
node server.js # lancer server backend
cd ../frontend/
yarn install #install dependencies
yarn serve #lancer server front
npm run storybook # lancer storybook
```

OR

```shell
cd api/ && npm run start # it will start concurrently the backend server + frontend server
npm run storybook
```
